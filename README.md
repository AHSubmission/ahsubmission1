Hi,

This repository contains an Excel file with data about 292 gathered use cases from 4 focus groups involving 21 participants.

The focus groups focused on gathering use cases regarding daily usage of Supernumerary Robotic Limbs. 

The file contains 5 tabs. The first tab includes an overal summary and categorization of all use cases. Tabs 2-5 contain a classified list of use cases within each focus group.

This file is provided as companion to our submission to Augmented Human Conference 2019. 

Thank you,


